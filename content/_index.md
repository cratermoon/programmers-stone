+++
title = "The Programmers' Stone: Revisited"
type = 'docs'
+++

The Programmers' Stone: Revisited is a remastering of the original from 1997-1999.
Credit for the original content is Copyright © Alan G Carter and Colston Sanger 1997.

I've lightly edited the original to fix a few spelling and formatting issues.
Where British spelling was used, it has been changed to US.
Any errors or alterations differing from the original are the fault of the editor,
send comments or corrections to me at <sen@cmdev.com>

----

Hi, and welcome to The Programmers' Stone. The purpose of this site is
to recapture, explore and celebrate the Art of Computer Programming. By
so doing we hope to help the reader either become a better programmer,
understand what less experienced programmers are struggling with, or
communicate more effectively with other experienced programmers.

We know from work with individuals that by doing this we put the fun
back into the work and greatly extend the boundaries of the possible, so
building much smarter and stronger systems.

The present structure is planned around an eight day course, delivered
two days a week for four weeks. Each chapter corresponds to the course
notes for one day's material. The eighth day should be free discussion,
so no prepared notes, meaning that there are seven chapters. We've
deliberately made each chapter a single HTML page because it makes it
much easier to print the text. Sorry there are no internal anchors yet,
there are big headings, so use your slider!

We'd very much like to hear from you!

Alan & Colston

<alan@melloworld.com>

<colston@shotters.dircon.co.uk>
