+++
type = 'docs'
weight = 90
title = 'Extreme Programming'
+++
From: Colston Sanger colston@shotters.dircon.co.uk

Kent Beck's new *Extreme Programming explained: embrace change*
(Addison-Wesley) arrived from Amazon this morning.

From the Preface:

> To some folks, XP seems like just good common sense. So why the
> "extreme" in the name? XP takes commonsense principles and practices
> to extreme levels.
>
> -   If code reviews are good, we'll review code all the time (pair
>     programming)
>
> -   If testing is good, everybody will test all the time (unit
>     testing), even the customers (functional testing)
>
> -   If design is good, we'll make it part of everybody's daily
>     business (refactoring)
>
> -   If simplicity is good, we'll always leave the system with the
>     simplest design that supports its current functionality (the
>     simplest thing that could possibly work)
>
> -   If architecture is important, everybody will work defining and
>     refining the architecture all the time (metaphor)
>
> -   If integration testing is important, then we'll integrate and
>     test several times a day (continuous integration)
>
> -   If short iterations are good, we'll make the iterations really,
>     really short - seconds and minutes and hours, not weeks and months
>     and years (the Planning Game).

I've had a lot of contact with XP people over the last few months and
the ideas make good sense to me. They push on from where we left the
Programmer's Stone in late 1997.
