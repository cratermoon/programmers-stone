+++
type = 'docs'
weight = 80
title = 'Unsolicited Testimonial'
+++

Well… :-)

------------------------------------------------------------------------

From: "Philip W. Darnowsky" <pdarnows@qis.net>

On Tue, 2 Nov 1999, Alan Carter wrote:

> Slogan: The Programmers' Stone improves your sex life. This is not an
> abstract ethical argument I know, but it's real.

I will attest to this. Until a few weeks ago, I worked in a highly
packerly environment. I was doing software development, but it was for a
large government contractor, so the packers were running the place. When
I quit, and took the job I now have in a place run by mappers, I started
to notice a feeling that I hadn't had for quite a while. Lust. My sex
drive has since recovered to its natural level.
